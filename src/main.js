import Vue from 'vue';

import VueRouter from 'vue-router';
Vue.use(VueRouter);

import VueAxios from 'vue-axios';
import axios from 'axios';
Vue.use(VueAxios, axios);

import App from './App.vue';
import inputForm from './components/inputForm.vue';
// import DisplayUser from './components/DisplayUser.vue';
// import EditUser from './components/EditUser.vue';

// import  { store }  from './store/';

// import VueResourse from 'vue-resource';
// Vue.use(VueResourse);

const routes = [
  {
        name: 'inputForm',
        path: '/',
        component: inputForm
  },
//   {
//         name: 'DisplayUser',
//         path: '/admin',
//         component: DisplayUser
//   },
//   {
//         name: 'EditUser',
//         path: '/edit/:id',
//         component: EditUser
//   }
];

const router = new VueRouter({ mode: 'history', routes });

//instatinat the vue instance
new Vue({
  //define the selector for the root component
  el: '#app',
  //pass the template to the root component
  template: '<App/>',
  //declare components that the root component can access
  components: { App },
  //pass in the router to the vue instance
  router  
}).$mount('#app')//mount the router on the app

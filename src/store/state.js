export default {    
    policy: {

    },
    
    profile: {    
        agent_code: null,
        country_id: 1,
        insurance_type_id: null,
        insurance_type: {},
        cover_type_id: null,
        cover_type: null,
        coverage_period: 12,
        current_section: null,      
        installments: 1,
        ipf_id: 1,
        provider_id: null,
        provider: {},
        selected_quotes: [],
        drop_off_lat: null,
        drop_off_long: null,
        delivery_datetime: null,

        // vehicle
        vehicle_use_id: null,
        vehicle_use: null,
        sum_insured_on_vehicle: null,
        vehicle_year_of_manufacture: null,
        vehicle_seating_capacity: null,
        tare_weight: null,   
        vehicle_registration_status: null,   
        registration_number: null,                            
        // health
        date_of_birth: null,
        gender: null,
        spouse_date_of_birth: null,
        dependents_below_19_years: 0,
        dependents_below_25_years: 0,
        cover_limit: 0,

        // travel
        specify_travelers: [],
        adult_travelers: 0,
        adults : [],
        country_travel : "",
        reason_travel : "",
        departure_date: "",
        return_date: "",

        // marine
        cargo_category: "",
        cargo_type: "",
        shipping_mode: "",
        packaging_type: "",
        sum_insured_on_marine: 0,

        // sme business
        sme_fire_perils: { 
            checked: 0,
            show: false,
            office_furniture: false,
            stock_for_trade: false,
            other_office: false
        },
        
        sme_building: {
            show: false,
            sum_insured_building: 0,
        },

        sme_business_interruption: {
            checked: 0,
            show: false,
            gross_profit: false,
            staff_wages: false,
            auditors_fees: false
        },
        sme_electronic_equipment: {
            checked: 0,
            show: false,
            office_computers: false,
            portable_laptop: false
        },
        sme_portable_office_items: {
            checked: 0,
            show: false,
            value_of_items_carried: 0
        },
        sme_burglary: {
            checked: 0,
            show: false,
            value_insured_event_barglary: 0,
            first_loss: 0
        },
        sme_money: {
            checked: 0,
            show: false,
            cash_in_transit: 0,
            cash_on_premise: 0,
            cash_in_safe: 0,
            damage_to_office: 0,
            cash_with_authorized: 0,
            estimated_cash: 0
        },
        sme_plate_glass: {
            checked: 0,
            show: false,
            glass_items: 0
        },
        sme_fidelity_guarantee: {
            checked: 0,
            show: false,
            fidelity_exposure: 0,
            aggredgated_annual: 0
        },
        sme_goods_in_transit: {
            checked: 0,
            show: false,
            vehicle_load: 0,
            estimated_annual_carry: 0
        },
        sme_public_liability : {
            checked: 0,
            show: false,
            limit_of_indemnity: 0
        },
        sme_wiba: {
            checked: 0,
            show: false,
            admin: 0,
            casual: 0
        },
        sme_group_personal: {
            checked: 0,
            show: false,
            admin: 0,
            casual: 0
        },
        sme_employer_liability: {
            checked: 0,
            show: false,
            option_A: 0,
            option_C: 0
        },
        sme_machine_break: {        
            show: false,
            machinery_breakdown: 0
        },
    },
    
    // -----------     step 4
    motorInsurance: {
        vehicle_current_situation: '',
        vehicle_registeration_number: '',
        vehicle_manufacturer: '',
        vehicle_model: ''       
    },

    golfersInsurance: {
        sport_club_name: '',
        territories_insurance: false,
        declined_proposal: false,
        bear_portion: false,
        increased_premium: false,
        refused_policy: false
    },

    homeInsurance: {
        plot_no: '',
        household_item_exceed: 'no',
        household_items: [],
        portable_item_exceed: 'no',
        portable_items: []
    },

    funeralInsurance : {
        principal_in_policy: "",
        national_id_number: "",
        marital_status: "",
        spouse_id: "",
        child_id: ""       
    },
 
    personalInsurance : {
        occupation: "",
        held_policy: "",
        insurer: "",
        health_status: "",
        health_reason: "",
        suffer_accident_3_years: "",
        accidents: "",
        national_id_number: "",
        phone: "",
        relationship: "",
        address: "",
        city:"",
        postal_code: ""        
    },

    healthDetails: {
        NHIF: "",
        myself: {
            first_name: "",
            last_name: "",
            gender: "",
            birthday: ""
        },
        spouse: {
            first_name: "",
            last_name: "",
            gender: "",
            birthday: ""
        },
        child: {
            first_name: "",
            last_name: "",
            gender: "",
            birthday: ""
        },            
        registration_number: null,
    },

    policies: [
        {
            profiles: [
                
            ],
        },
    ],

    user: {
        first_name: null,
        middle_name: null,
        last_name: null,
        company_name: null,
        email: null,
        mobile_number: null,
        postal_address: null,
        postal_code: null,
        location: {},
        location_id: null,
        city: null,
        agent_status: null,
        national_id: null,
        pin: null,
        IRA_registration_number: null,
        AKI_certificate_serial: null,
        password: null,
        password_confirmation: null,
        registered_agent: 0,        
        agent_status_details: null,
        underwriters: {},
    },

    quotes: [],
    insuranceTypes: [],        
    coverTypes: [], 
    optionalBenefits: [],              
    vehicleUses: [],                           
    years: [],  
    quote: {},
    showOptionalBenefitsDiv: false,
    checkedOptionalBenefits: [],
    token: localStorage.getItem('token'),
    loggedInUser: localStorage.getItem('logged_in_user'),
    agentPageState: '',
    vehicleRegistrationNumbers: [],
    locations: [],
    underwriters: [],        
    branches: [],
}
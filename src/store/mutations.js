const LOGIN = "LOGIN";
const LOGIN_SUCCESS = "LOGIN_SUCCESS";
const LOGOUT = "LOGOUT";

export default {
    setPolicy(state, payload) {
        state.profile = payload;
    },
    
    setProfile(state, payload) {
        state.profile = payload;
    },    
    
    setPolicies(state, payload) {
        state.policies = payload;
    },

    setPolicyProfile(state, payload) {
        state.policies[0].profiles[0] = payload
    },
    
    setUser(state, payload) {
        state.user = payload;
    },
    
    setProfileAgentCode(state, payload) {
        state.profile.agent_code = payload;
    },

    setAgent(state, payload) {
        state.agent = payload;
    },

    setMotorInsurace(state, payload) {
        state.motorInsurance = payload;
    },

    setGolfersInsurance(state, payload) {
        state.golfersInsurance = payload;
    },

    setHomeInsurance(state, payload) {
        state.homeInsurance = payload;
    },

    setFuneralInsurance(state, payload) {
        state.funeralInsurance = payload;
    },

    setPersonalInsurance(state, payload) {
        state.personalInsurance = payload;
    },

    setHealthDetails(state, payload) {
        state.healthDetails = payload;
    },

    setHealthStatus(state, payload) {
        state.healthStatus = payload;
    },

    setDelivery(state, payload) {
        state.delivery = payload;
    },

    setAgentPageState(state, payload) {
        state.agentPageState = payload;
    },

    setInsuranceTypes(state, payload) {
        state.insuranceTypes = payload;
    },

    setToken(state, payload){
        state.token = payload;        
    },

    setLoggedInUser(state, payload){
        state.loggedInUser = payload;        
    },

    setProfileInsuranceType(state, payload) {         
        state.profile.insurance_type = payload;
        state.profile.insurance_type_id = payload.id;
    },
    
    setProfileProvider(state, payload) {
        state.profile.provider = payload;
        state.profile.provider_id = payload.id;
    },
    
    setQuotes(state, payload) {
        state.quotes = payload;
    },

    setSelectedQuotes(state, payload) {
        state.profile.selected_quotes = payload;
    },

    setCoverTypes(state, payload) {
        state.coverTypes = payload;
    }, 

    setProfileCoverType(state, payload) {        
        state.profile.cover_type = payload;
        state.profile.cover_type_id = payload.id;
    }, 

    setVehicleUses(state, payload) {
        state.vehicleUses = payload;
    }, 
    
    setProfileVehicleUse(state, payload) {        
        state.profile.vehicle_use = payload;
        state.profile.vehicle_use_id = payload.id;
    }, 
    
    setProfileSumInsuredOnVehicle(state, payload) {        
        state.profile.cover_type_id = payload;
    }, 

    setYears(state) {
        let years = [];

        for (var y = new Date().getFullYear(); y >= new Date().getFullYear() - 20; y--) {
            years.push(y.toString());
        } 

        state.years = years;
    },

    setProfileVehicleYearOfManufacture(state, payload) {        
        state.profile.vehicle_year_of_manufacture = payload;
    },
    
    setProfileVehicleSeatingCapacity(state, payload) {        
        state.profile.vehicle_seating_capacity = payload;
    },      
    
    setProfileTareWeight(state, payload) {        
        state.profile.tare_weight = payload;
    },      
    
    setProfileCurrentSection(state, payload) {        
        state.profile.current_section = payload;
    },      
    
    setProfileProfileId(state, payload) {
        state.profile.profile_id = payload;
    },      

    setProfilePolicyId(state, payload) {
        state.profile.policy_id = payload;
    },    

    setProfileHome(state, payload) {
        state.profile.insure_building           = payload.insure_building;
        state.profile.insured_building_value    = payload.insured_building_value;
        state.profile.insure_household          = payload.insure_household;
        state.profile.insured_household_value   = payload.insured_household_value;
        state.profile.insure_portable_items     = payload.insure_portable_items;
        state.profile.insured_portable_value    =  payload.insured_portable_value;
        state.profile.insure_domestic_employees = payload.insure_domestic_employees;
        state.profile.indoor_employeers_num     = payload.indoor_employeers_num;
        state.profile.outdoor_employeers_num    = payload.outdoor_employeers_num;
    },

    setQuote(state, payload) {
        state.quote = payload;
    },

    setOptionalBenefits(state, payload) {
        state.optionalBenefits = payload;
    },              
    
    setShowOptionalBenefitsDiv(state, payload) {
        state.showOptionalBenefitsDiv = payload;
    },              
    
    setCheckedOptionalBenefits(state, payload) {
        state.checkedOptionalBenefits = payload;
    },

    setBranches(state, payload) {
        state.branches= payload;
    },

    [LOGIN](state) {
        state.pending = true;
    },

    [LOGIN_SUCCESS](state) {
        state.isLoggedIn = true;
        state.pending = false;
    },

    [LOGOUT](state) {
        state.isLoggedIn = false;
    },

    setVehicleRegistrationNumbers(state, payload) {
        state.vehicleRegistrationNumbers = payload;
    },

    setProfileRegistrationNumber(state, payload) {
        state.profile.registration_number = payload;
    },

    setLocations(state, payload) {
        state.locations = payload;
    },

    setUnderwriters(state, payload) {
        state.underwriters = payload;
    }
}
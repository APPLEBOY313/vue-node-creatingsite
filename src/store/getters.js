export default {
    user(state) {
        return state.user;
    },
    
    profile(state) {
        return state.profile;
    },

    policies(state) {
        return state.policies;
    },

    agent(state) {
        return state.agent;
    },

    motorInsurance(state) {
        return state.motorInsurance;
    },

    golfersInsurance(state) {
        return state.golfersInsurance;
    },

    homeInsurance(state) {
        return state.homeInsurance;
    },

    funeralInsurance(state) {
        return state.funeralInsurance;
    },

    personalInsurance(state) {
        return state.personalInsurance;
    },

    healthDetails(state) {
        return state.healthDetails;
    },

    healthStatus(state) {
        return state.healthStatus;
    },

    delivery(state) {
        return state.delivery;
    },

    agentPageState(state) {
        return state.agentPageState;
    },

    insuranceTypes(state) {
        return state.insuranceTypes;
    },
    
    coverTypes(state) {
        return state.coverTypes;
    },

    vehicleUses(state) {
        return state.vehicleUses;
    },

    quote(state) {
        return state.quote;
    },

    quotes(state) {
        return state.quotes;
    },

    optionalBenefits(state) {
        return state.optionalBenefits;
    },   
    
    years(state) {
        return state.years;
    },    
    
    showOptionalBenefitsDiv(state) {
        return state.showOptionalBenefitsDiv;
    },
    
    checkedOptionalBenefits(state) {
        return state.checkedOptionalBenefits;
    },
    
    token(state) {
        return state.token;
    },

    loggedInUser(state) {
        try {
            return JSON.parse(state.loggedInUser);
        } catch (e) {
            return state.loggedInUser;
        }        
    },
    
    vehicleRegistrationNumbers(state) {
        return state.vehicleRegistrationNumbers;
    },

    locations(state) {
        return state.locations;
    },

    underwriters(state) {
        return state.underwriters;
    },

    branches(state) {
        return state.branches;
    }
}
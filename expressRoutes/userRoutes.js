var express = require('express');
var app = express();
var userRoutes = express.Router();
var fs = require('fs');

// Require User model in our routes module
var User = require('../models/User');

// Defined store route
userRoutes.route('/create').post(function (req, res) {
	var source_url = "./scrape/" + req.body._id + "/" + req.body._id +".json";
	console.log(source_url);
	
	console.log("\n *STARTING* \n");
	// Get content from file
	var contents = fs.readFileSync(source_url);
	// Define to JSON type
	var jsonContent = JSON.parse(contents);
	// Get Value from JSON
	//  console.log("myFile:", jsonContent.listing.picture_captions[0]);
	console.log(jsonContent);
	res.json(jsonContent);
});

// Defined get data(index or listing) route
userRoutes.route('/').get(function (req, res) {
  User.find(function (err, users){
    if(err){
      console.log(err);
    }
    else {
      res.json(users);
    }
  });
});

// Defined edit route
userRoutes.route('/edit/:id').get(function (req, res) {
  var id = req.params.id;
  User.findById(id, function (err, user){
      res.json(user);
  });
});

//  Defined update route
userRoutes.route('/update/:id').post(function (req, res) {
  User.findById(req.params.id, function(err, user) {
    if (!user)
      return next(new Error('Could not load Document'));
    else {
      user.name = req.body.name;
      user.price = req.body.price;

      user.save().then(user => {
          res.json('Update complete');
      })
      .catch(err => {
            res.status(400).send("unable to update the database");
      });
    }
  });
});

// Defined delete | remove | destroy route
userRoutes.route('/delete/:id').get(function (req, res) {
  User.findByIdAndRemove({_id: req.params.id}, function(err, user){
		if(err) res.json(err);
		else res.json('Successfully removed');
	});
});

module.exports = userRoutes;

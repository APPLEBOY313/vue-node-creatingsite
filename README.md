# Nonfunctional requirements
bootstrap UI

## Installation instructions 
a) type the following command in your project root directory

--------------------------------------------
yarn install

yarn upgrade
--------------------------------------------

b) You need to install MongoDB database and also start the server of MongoDB

--------------------------------------------
ex) D:\mongodb\bin\mongod.exe --dbpath d:\mongodb\data
--------------------------------------------

c) You also need to start the NodeJS server by typing following command.

--------------------------------------------
nodemon server
--------------------------------------------

d) Go to the project folder -> config -> DB.js file and change the URI according to your database connection and credentials.

--------------------------------------------
npm run serve
--------------------------------------------

e) webpack development server will start at: http://localhost:8080
This project is simple one using vue.js and node.js.
(vue for front end and node  for backend)

### Architecture discussion
This is a basic signup and administrator Application. Which utilizes frontend of VueJS and backend of Node.js.

#### Plan of action

To signup page:  http://localhost:8080/
To administrator page:  http://localhost:8080/admin